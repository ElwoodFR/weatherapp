package info.dupas.android.meteo;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class OWM implements JSONProvider<Observation>{

    private final static String URL = "https://api.openweathermap.org/data/2.5/group?id=2950159,2761369,3169070,3117735,2988507,756135,2673730,2618425,658225,3196359,264371,2800866,456172,2267057,3054643,2964574,588409,3067696,593116,3060972&units=metric&lang=fr&mode=json";
    private final static String KEY = "9b7f85bb1af08b932959a75449edf9c5";
    private final static String ICON_BASE_URL = "https://api.openweathermap.org/img/w/";
    private final static String ICON_EXTENSION = "png";

    @Override
    public String getURL() {
        return URL + "&appid=" + KEY;
    }

    @Override
    public ArrayList<Observation> JSON2list(String strJSON) throws JSONException {
        //Créer l'ArrayList
        ArrayList<Observation> list = new ArrayList<>();
        //Récupérer la racine Json
        JSONObject root = new JSONObject(strJSON);
        JSONArray cities = root.getJSONArray("list");
        //Pour chaque "ville", extraire les données
        for (int i = 0; i < cities.length(); i++) {
            Observation observation = new Observation();
            list.add(observation);
            JSONObject objCity = cities.getJSONObject(i);
            JSONObject weather = objCity.getJSONArray("weather").getJSONObject(0);
            observation.description = weather.getString("description");
            observation.iconURL = ICON_BASE_URL + weather.getString("icon") + "." + ICON_EXTENSION;
            JSONObject main = objCity.getJSONObject("main");
            observation.min = (int) Math.round(main.getDouble("temp_min"));
            observation.max = (int) Math.round(main.getDouble("temp_max"));
            observation.city = objCity.getString("name");
        }
        return list;
    }
}
