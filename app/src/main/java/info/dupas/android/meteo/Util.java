package info.dupas.android.meteo;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.Network;
import android.net.NetworkCapabilities;

public final class Util {
    static boolean isConnected(final Context context) {
        // Récupérer un ConnectivityManager.
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        // Si null, pas de réseau disponible.
        if (cm == null)
            return false;
        // Sinon récupérer les réseau disponible
        Network[] networks = cm.getAllNetworks();
        // Parmi ces réseaux, rechercher une connexion internet.
        for (Network network : networks) {
            NetworkCapabilities nc = cm.getNetworkCapabilities(network);
            if (nc != null && nc.hasCapability(NetworkCapabilities.NET_CAPABILITY_INTERNET)) {
                return true;
            }
        }
        return false;
    }
}
