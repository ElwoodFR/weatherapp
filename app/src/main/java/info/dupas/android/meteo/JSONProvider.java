package info.dupas.android.meteo;

import org.json.JSONException;

import java.util.ArrayList;

public interface JSONProvider<T> {
    String getURL();

    ArrayList<T> JSON2list(String strJSON) throws JSONException;
}
