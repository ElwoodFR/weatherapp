package info.dupas.android.meteo;

import android.graphics.Bitmap;

import androidx.annotation.NonNull;

public class Observation {
    String city;
    int min;
    int max;
    String description;
    String iconURL;
    transient Bitmap icon;

    public Observation() {

    }

    @NonNull
    @Override
    public String toString() {
        return city + " : " + min + "°C / " + max + "°C";
    }

    
}
