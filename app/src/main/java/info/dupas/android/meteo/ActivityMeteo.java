package info.dupas.android.meteo;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.lifecycle.ViewModelProvider;

import com.google.android.material.snackbar.Snackbar;

public class ActivityMeteo extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // definir le layout
        setContentView(R.layout.activity_meteo);
        // Supporter l'Actionbar via Toolbar
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        //Récupérer une instance de ViewModelMeteo
        ViewModelMeteo vmMeteo = new ViewModelProvider(this).get(ViewModelMeteo.class);

        vmMeteo.getState().observe(this, state -> {
            switch (state){
                case ViewModelMeteo.DONE :
                    return;
                case ViewModelMeteo.NO_INTERNET_CONNECTION :
                    //Afficher un avertissement : Snackbar
                    Snackbar.make(findViewById(R.id.container), R.string.error_no_internet_connection, Snackbar.LENGTH_LONG).show();
                    break;
            }
            vmMeteo.setState(ViewModelMeteo.DONE);
        });

        // Si necessaire, charger une instance de FragmentList
        // Commence une transaction
        if(savedInstanceState == null)
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.container, new FragmentList())
                    .commit();

        /*  Autre version
        // commence une transaction
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        // Remplace tout ce qui se trouve dans la vue container par ce fragment
        ft.replace(R.id.container, new FragmentList());
        // commit la tansaction
        ft.commit();
        */
    }
}
