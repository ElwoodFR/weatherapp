package info.dupas.android.meteo;

import android.app.Application;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import org.json.JSONException;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.Scanner;

public final class ViewModelMeteo extends AndroidViewModel {

    private Application application;
    final static  String NO_INTERNET_CONNECTION = "NO_INTERNET_CONNECTION";
    final static  String DONE = "DONE";
    // propiété string encapsuler dans MutableLiveData (donnée sauvegarder en cas de déchargerment et observer en exterieur)
    private MutableLiveData<ArrayList<Observation>> observations;
    private MutableLiveData<String> state = new MutableLiveData<>();

    public ViewModelMeteo(@NonNull Application application) {
        super(application);
        this.application = application;
    }

    LiveData<String> getState() {
        return state;
    }

    void setState(final String state) {
        this.state.setValue(state);
    }

    LiveData<ArrayList<Observation>> getStrJSON(){
        // Si StrJSON null, en gros si pas encore de liveData => instancier et charger
        if (observations == null){
            observations = new MutableLiveData<>();
            loadOWN();
        }
        return observations;
    }

    private void loadOWN(){
        // Envoyer la requete OWM
        if(Util.isConnected(application)) {
            Log.d("OWM", application.getString(R.string.owm_loading));
            new AsyncTaskMeteo().execute(new OWM().getURL());
        } else {
            Toast.makeText(application, R.string.error_no_internet_connection, Toast.LENGTH_LONG).show();
            setState(NO_INTERNET_CONNECTION);
        }
    }

    private class AsyncTaskMeteo extends AsyncTask<String, Void, ArrayList<Observation>> {

        @Override
        protected ArrayList<Observation> doInBackground(String... strings) {
            // se connecter à OWM
            InputStream is;
            try{
                is = new URL(strings[0]).openStream();
            } catch (IOException e){
                Log.e("OWN", e.getMessage());
                return null;
            }
            // Lire ligne à ligne la réponse
            StringBuilder sbJSON = new StringBuilder(10000);
            Scanner scanner = new Scanner(is); // Scanner pour recupérer un fichier
            while (scanner.hasNextLine())
                sbJSON.append(scanner.nextLine());
            ArrayList<Observation> list;
            try {
                list =  new OWM().JSON2list(sbJSON.toString());
            } catch (JSONException e) {
                Log.e("OWM", application.getString(R.string.error_owm_json_invalid));
                return null;
            }
            // Pour chaque observation, télécharger l'icône
            for (Observation observation : list) {
                InputStream isImage;
                try {
                    isImage = new URL(observation.iconURL).openStream();
                    observation.icon = BitmapFactory.decodeStream(isImage);
                } catch (IOException e) {
                    Log.e("OWM", application.getString(R.string.error_icon_owm_not_found));
                }
            }
            return list;
        }

        @Override
        protected void onPostExecute(ArrayList<Observation> list) {
            observations.setValue(list);
        }
    }

}
