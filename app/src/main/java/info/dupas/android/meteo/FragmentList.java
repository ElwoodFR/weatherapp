package info.dupas.android.meteo;


import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentList extends Fragment {

    private ViewModelMeteo vmMteo;

   public FragmentList() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Récuperer une instance de ViewModelMeteo
        vmMteo = new ViewModelProvider(requireActivity()).get(ViewModelMeteo.class); // toutes les class en java on une proprité class qui fait référence à elle même.

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflater le layout
        View view = inflater.inflate(R.layout.fradment_list, container, false);
        // 3 parametres au lieu de 2 car on ne veut pas attacher à la racine du parent
        // container = framelayout
        // Viewgroupe est activityMeteo
        // On le met false pour ne pas l'

        // Récupérer le composant listView depuis le layout
        ListView listView = view.findViewById(R.id.list); // retourne le composant listView
        // créer un ArrayAdapter de string dans une variable
        ArrayAdapter<String> adapter = new ArrayAdapter<>(getContext(), R.layout.item); // item est le nom du layout
        // Associer l'Adapter à la listView. Ces 2 là vont travailler ensemble.
        listView.setAdapter(adapter);
        vmMteo.getStrJSON().observe(getViewLifecycleOwner(),observations -> {
            Log.d("OWM", observations.toString());
            adapter.clear();
            ArrayList<String> obs = new ArrayList<>();
            for(Observation observation : observations)
                adapter.add(observation.toString());
        });
        return view;
    }


}
